package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"

	firebase "firebase.google.com/go"
	"github.com/guygrigsby/yard-site/cmd/video/ftp"
	"github.com/inconshreveable/log15"
	"google.golang.org/api/option"
)

var (
	credsLoc string
	ftpPort  int
)

func main() {

	log := log15.New()
	log.SetHandler(log15.MultiHandler(
		log15.StreamHandler(os.Stderr, log15.LogfmtFormat()),
		log15.Must.FileHandler("/var/log/goftp.log", log15.LogfmtFormat())))

	log.Info("starting yard-server")

	flag.StringVar(&credsLoc, "creds", "", "location of the credentials file")
	flag.IntVar(&ftpPort, "ftp", 8021, "FTP port to listen on")

	opt := option.WithCredentialsFile(credsLoc)
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		log.Error(
			"error initializing app",
			"err", err,
		)
	}

	go handleFTP(ftpPort, app, log)

	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir("/home/camera/pub/YARD")))
	mux.Handle("/video", handleVideo(log, app))

	if err := http.ListenAndServe(":8080", mux); err != nil {
		log.Error(
			"yard-server failed",
			"err", err,
		)
	}
}

func handleFTP(port int, app *firebase.App, log log15.Logger) {
	server := fmt.Sprintf(":%d", port)
	listener, err := net.Listen("tcp", server)
	if err != nil {
		log.Error("failure", "err", err)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Error("failure", "err", err)
			continue
		}
		go handleConn(conn, log)
	}
}

func handleConn(c net.Conn, log log15.Logger) {
	defer c.Close()
	ftp.Serve(ftp.NewConn(c, "/home/guy/ftptest/"), log)
}
func handleVideo(log log15.Logger, app *firebase.App) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
	}

}
