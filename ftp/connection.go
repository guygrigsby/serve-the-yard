package ftp

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"path/filepath"
	"strings"

	"github.com/inconshreveable/log15"
)

type dataType int

const (
	status150 = "150 File status okay; about to open data connection."
	status200 = "200 Command okay."
	status202 = "202 Command not implemented, superfluous at this site."
	status220 = "220 Service ready for new user."
	status221 = "221 Service closing control connection."
	status226 = "226 Closing data connection. Requested file action successful."
	status230 = "230 User %s logged in, proceed."
	status250 = "250 Requested file action okay, completed."
	status257 = "257 \"%s\" created."
	status350 = "350 Requested file action pending further information."
	status425 = "425 Can't open data connection."
	status426 = "426 Connection closed; transfer aborted."
	status501 = "501 Syntax error in parameters or arguments."
	status502 = "502 Command not implemented."
	status504 = "504 Cammand not implemented for that parameter."
	status550 = "550 Requested action not taken. File unavailable."

	ascii dataType = iota
	binary
)

func (c *Conn) dataConnect() (net.Conn, error) {
	conn, err := net.Dial("tcp", c.dataPort.toAddress())
	if err != nil {
		return nil, err
	}
	return conn, nil
}

type dataPort struct {
	h1, h2, h3, h4 int // host
	p1, p2         int // port
}

func dataPortFromHostPort(hostPort string) (*dataPort, error) {
	var dp dataPort
	_, err := fmt.Sscanf(hostPort, "%d,%d,%d,%d,%d,%d",
		&dp.h1, &dp.h2, &dp.h3, &dp.h4, &dp.p1, &dp.p2)
	if err != nil {
		return nil, err
	}
	return &dp, nil
}

func (d *dataPort) toAddress() string {
	if d == nil {
		return ""
	}
	// convert hex port bytes to decimal port
	port := d.p1<<8 + d.p2
	return fmt.Sprintf("%d.%d.%d.%d:%d", d.h1, d.h2, d.h3, d.h4, port)
}

// Conn represents a connection to the FTP server
type Conn struct {
	conn     net.Conn
	dataType dataType
	dataPort *dataPort
	rootDir  string
	workDir  string
}

// NewConn returns a new FTP connection
func NewConn(conn net.Conn, rootDir string) *Conn {
	return &Conn{
		conn:    conn,
		rootDir: rootDir,
		workDir: "/",
	}
}
func (c *Conn) allo(args []string, log log15.Logger) {
	c.respond(status202)
}
func (c *Conn) user(args []string) {
	c.respond(fmt.Sprintf(status230, strings.Join(args, " ")))
}
func (c *Conn) pwd(args []string) {
	c.respond(fmt.Sprintf(status257, c.workDir))
}
func (c *Conn) mkd(args []string, log log15.Logger) {
	err := os.Mkdir(args[0], 0777)
	if err != nil {
		log.Error(
			"failed to create dir",
			"err", err,
		)
		c.respond(status550)
		return
	}

}

var (
	moves []string
)

func (c *Conn) renameTo(args []string, log log15.Logger) {
	// queue it up
	newLoc := filepath.Join(c.workDir, args[0])
	dst := filepath.Join(c.rootDir, newLoc)
	_, err := Copy(moves[0], dst)
	if err != nil {
		log.Error(
			"move failed",
			"err", err,
		)
		c.respond(status550)
		return
	}

}
func (c *Conn) renameFrom(args []string) {
	// queue it up
	workDir := filepath.Join(c.workDir, args[0])
	absPath := filepath.Join(c.rootDir, workDir)
	moves = append(moves, absPath)
}
func (c *Conn) cwd(args []string) {
	if len(args) != 1 {
		c.respond(status501)
		return
	}

	workDir := filepath.Join(c.workDir, args[0])
	absPath := filepath.Join(c.rootDir, workDir)
	_, err := os.Stat(absPath)
	if err != nil {
		log.Print(err)
		c.respond(status550)
		return
	}
	c.workDir = workDir
	c.respond(status200)
}

func (c *Conn) list(args []string) {
	var target string
	if len(args) > 0 {
		target = filepath.Join(c.rootDir, c.workDir, args[0])
	} else {
		target = filepath.Join(c.rootDir, c.workDir)
	}

	files, err := ioutil.ReadDir(target)
	if err != nil {
		log.Print(err)
		c.respond(status550)
		return
	}
	c.respond(status150)

	dataConn, err := c.dataConnect()
	if err != nil {
		log.Print(err)
		c.respond(status425)
		return
	}
	defer dataConn.Close()

	for _, file := range files {
		_, err := fmt.Fprint(dataConn, file.Name(), c.EOL())
		if err != nil {
			log.Print(err)
			c.respond(status426)
		}
	}
	_, err = fmt.Fprintf(dataConn, "%v", c.EOL())
	if err != nil {
		log.Print(err)
		c.respond(status426)
	}

	c.respond(status226)
}

// respond copies a string to the client and terminates it with the appropriate FTP line terminator
// for the datatype.
func (c *Conn) respond(s string) {
	log.Print(">> ", s)
	_, err := fmt.Fprint(c.conn, s, c.EOL())
	if err != nil {
		log.Print(err)
	}
}

// EOL returns the line terminator matching the FTP standard for the datatype.
func (c *Conn) EOL() string {
	switch c.dataType {
	case ascii:
		return "\r\n"
	case binary:
		return "\n"
	default:
		return "\n"
	}
}

func (c *Conn) retr(args []string) {
	if len(args) != 1 {
		c.respond(status501)
		return
	}

	path := filepath.Join(c.rootDir, c.workDir, args[0])
	file, err := os.Open(path)
	if err != nil {
		log.Print(err)
		c.respond(status550)
	}
	c.respond(status150)

	dataConn, err := c.dataConnect()
	if err != nil {
		log.Print(err)
		c.respond(status425)
	}
	defer dataConn.Close()

	_, err = io.Copy(dataConn, file)
	if err != nil {
		log.Print(err)
		c.respond(status426)
		return
	}
	_, err = io.WriteString(dataConn, c.EOL())
	if err != nil {
		log.Print(err)
		c.respond(status426)
		return
	}
	c.respond(status226)
}

func (c *Conn) setDataType(args []string) {
	if len(args) == 0 {
		c.respond(status501)
	}

	switch args[0] {
	case "A":
		c.dataType = ascii
	case "I": // image/binary
		c.dataType = binary
	default:
		c.respond(status504)
		return
	}
	c.respond(status200)
}

func (c *Conn) port(args []string) {
	if len(args) != 1 {
		c.respond(status501)
		return
	}
	dataPort, err := dataPortFromHostPort(args[0])
	if err != nil {
		log.Print(err)
		c.respond(status501)
		return
	}
	c.dataPort = dataPort
	c.respond(status200)
}
