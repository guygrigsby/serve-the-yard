// Package ftp provides structs and functions for running an FTP server.
package ftp

import (
	"bufio"
	"strings"

	"github.com/inconshreveable/log15"
)

/*
Feb 23 15:14:44 localhost pure-ftpd[65523]: (camera@192.168.0.104) [DEBUG] Command [pwd] []
Feb 23 15:14:44 localhost pure-ftpd[65523]: (camera@192.168.0.104) [DEBUG] Command [type] [I]
Feb 23 15:14:44 localhost pure-ftpd[65523]: (camera@192.168.0.104) [DEBUG] Command [pwd] []
Feb 23 15:14:44 localhost pure-ftpd[65523]: (camera@192.168.0.104) [DEBUG] Command [cwd] [/]
Feb 23 15:14:44 localhost pure-ftpd[65523]: (camera@192.168.0.104) [DEBUG] Command [cwd] [/pub/YARD/2021-02-23/001/dav/15]
Feb 23 15:14:44 localhost pure-ftpd[65523]: (camera@192.168.0.104) [DEBUG] Command [rnfr] [15.14.36-15.14.36[M][0@0][0].idx_]
Feb 23 15:14:44 localhost pure-ftpd[65523]: (camera@192.168.0.104) [DEBUG] Command [rnto] [15.14.36-15.14.44[M][0@0][0].idx]
Feb 23 15:14:44 localhost pure-ftpd[65523]: (camera@192.168.0.104) [NOTICE] File successfully renamed or moved: [15.14.36-15.14.36[M][0@0][0].idx_]->[15.14.36-15.14.44[M][0@0][0].idx]
Feb 23 15:14:44 localhost pure-ftpd[65523]: (camera@192.168.0.104) [DEBUG] Command [cwd] [/]
Feb 23 15:14:44 localhost pure-ftpd[65523]: (camera@192.168.0.104) [DEBUG] Command [quit] []
*/
// Serve scans incoming requests for valid commands and routes them to handler functions.
func Serve(c *Conn, log log15.Logger) {
	c.respond(status220)

	s := bufio.NewScanner(c.conn)
	for s.Scan() {
		input := strings.Fields(s.Text())
		if len(input) == 0 {
			continue
		}
		command, args := input[0], input[1:]
		log.Debug(
			"Incoming",
			"command", command,
			"args", args,
		)

		switch command {
		case "CWD": // cd
			c.cwd(args)
		case "PWD":
			c.pwd(args)
		case "MKD":
			c.mkd(args, log)
		case "LIST": // ls
			c.list(args)
		case "PORT":
			c.port(args)
		case "USER":
			c.user(args)
		case "QUIT": // close
			c.respond(status221)
			return
		case "RETR": // get
			c.retr(args)
		case "TYPE":
			c.setDataType(args)
		case "RNFR":
			c.renameFrom(args)
		case "RNTO":
			// TODO should check for prev RNFR
			c.renameTo(args, log)
		case "ALLO":
			c.allo(args, log)
		default:
			c.respond(status502)
		}
	}
	if s.Err() != nil {
		log.Error("failure", "err", s.Err())
	}
}
