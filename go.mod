module github.com/guygrigsby/yard-site/cmd/video

go 1.15

require (
	cloud.google.com/go/firestore v1.4.0 // indirect
	firebase.google.com/go v3.13.0+incompatible
	github.com/angusgmorrison/ftpeasy v0.0.0-20201029193408-6e754c690573 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/inconshreveable/log15 v0.0.0-20201112154412-8562bdadbbac
	github.com/mattn/go-colorable v0.1.8 // indirect
	google.golang.org/api v0.40.0
)
