import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { css } from 'pretty-lights'
import HomePage from './pages/HomePage.jsx'
import './App.css'
const container = css`
  display: flex;
  flex-direction: column;
`
const App = () => {
  return (
    <Router>
      <div className={container}>
        <nav></nav>
        <header></header>
        <Switch>
          <Route exact path="/">
            <HomePage/>
          </Route>
          <Route path="/:year/:month/:day/:hour"></Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App
