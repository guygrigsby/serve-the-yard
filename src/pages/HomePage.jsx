import React from 'react'
import { css } from 'pretty-lights'
import PropTypes from 'prop-types'
const box = css`
  display: flex;
  flex-direction: column;
  padding: 2em;
`
const HomePage = ({setClip}) => {
  const [selected, setSelected] = React.useState('2021-02-23')

  const handleSubmit=(e)=> {
    e.stopPropagation()
    e.preventDefault()
    setClip(selected)
  }

  return (
    <div className={box}>
      <form onSubmit={handleSubmit}>
        <label>Select Date</label>
        <input type="date" id="recorded-date" name="recorded" value={selected} onChange={(e)=>setSelected(e.target.value)}/>
      </form>
    </div>
  )
}

HomePage.propTypes = {
  setClip: PropTypes.string,
}


export default HomePage
